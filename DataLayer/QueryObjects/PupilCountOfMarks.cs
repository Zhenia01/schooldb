namespace DataLayer.QueryObjects
{
    public class PupilCountOfMarks
    {
        public int Grade { get; set; }
        public int Count { get; set; }
    }
}