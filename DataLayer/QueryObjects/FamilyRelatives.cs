namespace DataLayer.QueryObjects
{
    public class FamilyRelatives
    {
        public int RelCode { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Type { get; set; }
    }
}