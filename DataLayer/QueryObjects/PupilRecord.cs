namespace DataLayer.QueryObjects
{
    public class PupilRecord
    {
        public int PupCode { get; set; }
        public int RecordCode { get; set; }

        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        
        public bool Attendance { get; set; }
        public int? Grade { get; set; }
        public string Comment { get; set; }
    }
}