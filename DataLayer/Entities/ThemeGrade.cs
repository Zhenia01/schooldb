namespace DataLayer.Entities
{
    public class ThemeGrade
    {
        public int ThemeCode { get; set; }
        public int PupCode { get; set; }
        public int DisCode { get; set; }
        public int Grade { get; set; }
    }
}