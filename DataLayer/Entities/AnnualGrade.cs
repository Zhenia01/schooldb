namespace DataLayer.Entities
{
    public class AnnualGrade
    {
        public int GradeCode { get; set; }
        public int PupCode { get; set; }
        public string SubjName { get; set; }
        public int AcademYear { get; set; }
        public int DateYear { get; set; }
        public string Grade { get; set; }
        public string Note { get; set; }
    }
}