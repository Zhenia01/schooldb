namespace DataLayer.Entities
{
    public class TeacherEmails
    {
        public string Email { get; set; }
        public int TeacherCode { get; set; }
    }
}