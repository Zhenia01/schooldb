namespace DataLayer.Entities
{
    public class Family
    {
        public int PupCode { get; set; }
        public int RelCode { get; set; }
        public string Type { get; set; }
    }
}