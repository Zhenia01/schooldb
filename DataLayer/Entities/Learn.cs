namespace DataLayer.Entities
{
    public class Learn
    {
        public int PupCode { get; set; }
        public int DisCode { get; set; }
        public int? SemesterGrade { get; set; }
    }
}