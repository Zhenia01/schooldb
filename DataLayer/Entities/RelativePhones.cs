namespace DataLayer.Entities
{
    public class RelativePhones
    {
        public string PhoneNum { get; set; }
        public int RelCode { get; set; }
    }
}