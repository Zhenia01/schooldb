using System;

namespace DataLayer.Entities
{
    public class Lesson
    {
        public int LesCode { get; set; }
        public int DisCode { get; set; }
        public int? TeacherCode { get; set; }
        public int Num { get; set; }
        public DateTime LessonDate { get; set; }
        public LessonType LessonType { get; set; }
        public string Theme { get; set; }
        public string HomeTask { get; set; }
    }

    public enum LessonType : byte
    {
        Independent,
        Test,
        Reading,
        Сlass
    }
}