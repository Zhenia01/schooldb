namespace DataLayer.Entities
{
    public class TeacherPhones
    {
        public string PhoneNum { get; set; }
        public int TeacherCode { get; set; }
    }
}