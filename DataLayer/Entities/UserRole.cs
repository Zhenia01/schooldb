namespace DataLayer.Entities
{
    public class UserRole
    {
        public string Login { get; set; }
        public string Name { get; set; }
    }
}