namespace DataLayer.Entities
{
    public class Relative
    {
        public int RelCode { get; set; }
        public string Login { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
    }
}