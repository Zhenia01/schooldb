namespace DataLayer.Entities
{
    public class Teacher
    {
        public int TeacherCode { get; set; }
        public string Login { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
    }
}