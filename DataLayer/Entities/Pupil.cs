using System;

namespace DataLayer.Entities
{
    public class Pupil
    {
        public int PupCode { get; set; }
        public string Login { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Address { get; set; }
    }
}