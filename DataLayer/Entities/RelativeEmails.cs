namespace DataLayer.Entities
{
    public class RelativeEmails
    {
        public string Email { get; set; }
        public int RelCode { get; set; }
    }
}