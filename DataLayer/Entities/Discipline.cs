namespace DataLayer.Entities
{
    public class Discipline
    {
        public int DisCode { get; set; }
        public string SubjName { get; set; }
        public int Group { get; set; }
        public int DateYear { get; set; }
        public int AcademYear { get; set; }
        public int Semester { get; set; }
    }
}