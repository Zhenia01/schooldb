namespace DataLayer.Entities
{
    public class Record
    {
        public int RecordCode { get; set; }
        public int PupCode { get; set; }
        public int LesCode { get; set; }
        public bool Attendance { get; set; }
        public int? Grade { get; set; }
        public string Comment { get; set; }
    }
}