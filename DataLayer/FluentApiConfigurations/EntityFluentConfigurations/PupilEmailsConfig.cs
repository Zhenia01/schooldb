using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class PupilEmailsConfig : IEntityTypeConfiguration<PupilEmails>
    {
        public void Configure(EntityTypeBuilder<PupilEmails> entity)
        {
            entity.HasKey(e => e.Email);

            entity.Property(e => e.Email).IsRequired().HasMaxLength(30);

            entity.HasOne<Pupil>().WithMany().HasForeignKey(e => e.PupCode).IsRequired();

            entity.ToTable("PupilEmails");
        }
    }
}