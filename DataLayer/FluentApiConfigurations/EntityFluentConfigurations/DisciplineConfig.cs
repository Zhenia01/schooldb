using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class DisciplineConfig : IEntityTypeConfiguration<Discipline>
    {
        public void Configure(EntityTypeBuilder<Discipline> entity)
        {
            entity.HasKey(d => d.DisCode);
            
            entity.Property(d => d.SubjName).IsRequired().HasMaxLength(20);
            entity.Property(d => d.DateYear).HasColumnType("SMALLINT");
            entity.Property(d => d.AcademYear).HasColumnType("TINYINT");
            entity.Property(d => d.Semester).HasColumnType("TINYINT");
            entity.Property(d => d.Group).HasColumnType("TINYINT");

            entity.HasOne<Subject>().WithMany().HasForeignKey(d => d.SubjName).IsRequired();

            entity.ToTable("Discipline");
        }
    }
}