using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class PupilConfig : IEntityTypeConfiguration<Pupil>
    {
        public void Configure(EntityTypeBuilder<Pupil> entity)
        {
            entity.HasKey(p => p.PupCode);
            entity.HasAlternateKey(p => p.Login);
                     
            entity.Property(p => p.Login).IsRequired().HasMaxLength(10);
            // entity.Property(p => p.Password).IsRequired();
            entity.Property(p => p.LastName).IsRequired().HasMaxLength(25);
            entity.Property(p => p.FirstName).IsRequired().HasMaxLength(25);
            entity.Property(p => p.MiddleName).HasMaxLength(25);
            entity.Property(p => p.BirthDate).HasColumnType("DATE");
            entity.Property(p => p.Address).IsRequired().HasMaxLength(50);

            entity.HasOne<User>().WithOne().HasForeignKey<Pupil>(u => u.Login).IsRequired();

            

            entity.ToTable("Pupil");
        }
    }
}