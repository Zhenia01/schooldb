using System.Runtime.InteropServices.ComTypes;
using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class UserRoleConfig : IEntityTypeConfiguration<UserRole> 
    {
        public void Configure(EntityTypeBuilder<UserRole> entity)
        {
            entity.HasKey(ur => new {ur.Login, ur.Name});

            entity.Property(ur => ur.Name).IsRequired().HasMaxLength(10);
            entity.Property(ur => ur.Login).IsRequired().HasMaxLength(10);
            
            entity.HasOne<User>().WithMany().HasForeignKey(ur => ur.Login).IsRequired();
            entity.HasOne<Role>().WithMany().HasForeignKey(ur => ur.Name).IsRequired();

            entity.ToTable("UserRole");
        }
    }
}