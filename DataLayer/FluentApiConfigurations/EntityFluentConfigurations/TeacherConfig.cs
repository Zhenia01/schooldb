using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class TeacherConfig : IEntityTypeConfiguration<Teacher>
    {
        public void Configure(EntityTypeBuilder<Teacher> entity)
        {
            entity.HasKey(p => p.TeacherCode);
            entity.HasAlternateKey(p => p.Login);
                     
            entity.Property(p => p.Login).IsRequired().HasMaxLength(10);
            // entity.Property(p => p.Password).IsRequired();
            entity.Property(p => p.LastName).IsRequired().HasMaxLength(25);
            entity.Property(p => p.FirstName).IsRequired().HasMaxLength(25);
            entity.Property(p => p.MiddleName).HasMaxLength(25);
            
            entity.HasOne<User>().WithOne().HasForeignKey<Teacher>(t => t.Login).IsRequired();

            entity.ToTable("Teacher");
        }
    }
}