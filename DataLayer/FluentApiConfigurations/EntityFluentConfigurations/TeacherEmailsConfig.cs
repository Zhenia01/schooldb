using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class TeacherEmailsConfig : IEntityTypeConfiguration<TeacherEmails>
    {
        public void Configure(EntityTypeBuilder<TeacherEmails> entity)
        {
            entity.HasKey(e => e.Email);

            entity.Property(e => e.Email).IsRequired().HasMaxLength(30);

            entity.HasOne<Teacher>().WithMany().HasForeignKey(e => e.TeacherCode).IsRequired();

            entity.ToTable("TeacherEmails");
        }
    }
}