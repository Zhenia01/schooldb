using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class RelativePhonesConfig : IEntityTypeConfiguration<RelativePhones>
    {
        public void Configure(EntityTypeBuilder<RelativePhones> entity)
        {
            entity.HasKey(e => e.PhoneNum);
            
            entity.Property(e => e.PhoneNum).IsRequired().HasMaxLength(10);

            entity.HasOne<Relative>().WithMany().HasForeignKey(e => e.RelCode).IsRequired();

            entity.ToTable("RelativePhones");
        }
    }
}