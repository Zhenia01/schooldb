using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class AnnualGradeConfig : IEntityTypeConfiguration<AnnualGrade>
    {
        public void Configure(EntityTypeBuilder<AnnualGrade> entity)
        {
            entity.HasKey(a => a.GradeCode);

            entity.Property(a => a.SubjName).IsRequired().HasMaxLength(20);
            entity.Property(a => a.AcademYear).HasColumnType("SMALLINT");
            entity.Property(a => a.DateYear).HasColumnType("SMALLINT");
            entity.Property(a => a.Grade).HasMaxLength(15);
            entity.Property(a => a.Note).HasMaxLength(30);

            entity.HasOne<Pupil>().WithMany().HasForeignKey(a => a.PupCode).IsRequired();
            entity.HasOne<Subject>().WithMany().HasForeignKey(a => a.SubjName).IsRequired();
        }
    }
}