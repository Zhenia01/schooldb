using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class RecordConfig : IEntityTypeConfiguration<Record>
    {
        public void Configure(EntityTypeBuilder<Record> entity)
        {
            entity.HasKey(r => r.RecordCode);

            entity.Property(r => r.Grade).HasColumnType("SMALLINT");
            entity.Property(r => r.Comment).HasMaxLength(100);

            entity.HasOne<Pupil>().WithMany().HasForeignKey(r => r.PupCode).IsRequired();
            entity.HasOne<Lesson>().WithMany().HasForeignKey(r => r.LesCode).IsRequired();
        }
    }
}