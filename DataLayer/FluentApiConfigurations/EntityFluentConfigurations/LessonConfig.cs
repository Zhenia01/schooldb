using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class LessonConfig : IEntityTypeConfiguration<Lesson>
    {
        public void Configure(EntityTypeBuilder<Lesson> entity)
        {
            entity.HasKey(l => l.LesCode);
            
            entity.Property(l => l.Num).HasColumnType("SMALLINT");
            entity.Property(l => l.LessonDate).HasColumnType("DATE");
            entity.Property(l => l.LessonType).HasConversion(
                    v => EnumToString(v),
                    s => StringToEnum(s))
                .HasMaxLength(15);
            entity.Property(l => l.Theme).IsRequired().HasMaxLength(25);
            entity.Property(l => l.HomeTask).HasMaxLength(100);
            
            entity.HasOne<Discipline>().WithMany().HasForeignKey(l => l.DisCode).IsRequired();
            entity.HasOne<Teacher>().WithMany().HasForeignKey(l => l.TeacherCode).OnDelete(DeleteBehavior.SetNull);
        }

        public static string EnumToString(LessonType v)
        {
            return v switch
            {
                LessonType.Independent => "Самостійна",
                LessonType.Reading => "Читання",
                LessonType.Test => "Контрольна",
                LessonType.Сlass => "Урок",
                _ => ""
            };
        }

        public static LessonType StringToEnum(string s)
        {
            return s switch
            {
                "Самостійна" => LessonType.Independent,
                "Читання" => LessonType.Reading,
                "Контрольна" => LessonType.Test,
                "Урок" => LessonType.Сlass,
                _ => LessonType.Сlass
            };
        }
    }
}