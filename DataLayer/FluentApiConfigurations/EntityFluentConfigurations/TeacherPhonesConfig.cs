using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class TeacherPhonesConfig : IEntityTypeConfiguration<TeacherPhones>
    {
        public void Configure(EntityTypeBuilder<TeacherPhones> entity)
        {
            entity.HasKey(e => e.PhoneNum);
            
            entity.Property(e => e.PhoneNum).IsRequired().HasMaxLength(10);

            entity.HasOne<Teacher>().WithMany().HasForeignKey(e => e.TeacherCode).IsRequired();

            entity.ToTable("TeacherPhones");
        }
    }
}