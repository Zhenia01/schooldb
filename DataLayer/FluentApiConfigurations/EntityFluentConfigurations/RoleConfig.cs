using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class RoleConfig : IEntityTypeConfiguration<Role> 
    {
        public void Configure(EntityTypeBuilder<Role> entity)
        {
            entity.HasKey(r => r.Name);

            entity.Property(r => r.Name).IsRequired().HasMaxLength(10);

            entity.ToTable("Role");
        }
    }
}