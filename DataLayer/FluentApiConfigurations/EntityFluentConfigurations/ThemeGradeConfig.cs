using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class ThemeGradeConfig : IEntityTypeConfiguration<ThemeGrade>
    {
        public void Configure(EntityTypeBuilder<ThemeGrade> entity)
        {
            entity.HasKey(t => t.ThemeCode);

            entity.Property(t => t.Grade).HasColumnType("SMALLINT");

            entity.HasOne<Discipline>().WithMany().HasForeignKey(d => d.DisCode).IsRequired();
        }
    }
}