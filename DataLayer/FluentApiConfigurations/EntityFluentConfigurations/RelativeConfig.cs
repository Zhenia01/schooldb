using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class RelativeConfig : IEntityTypeConfiguration<Relative>
    {
        public void Configure(EntityTypeBuilder<Relative> entity)
        {
            entity.HasKey(p => p.RelCode);
            entity.HasAlternateKey(p => p.Login);
                     
            entity.Property(p => p.Login).IsRequired().HasMaxLength(10);
            entity.Property(p => p.LastName).IsRequired().HasMaxLength(25);
            entity.Property(p => p.FirstName).IsRequired().HasMaxLength(25);
            entity.Property(p => p.MiddleName).HasMaxLength(25);
            
            entity.HasOne<User>().WithOne().HasForeignKey<Relative>(r => r.Login).IsRequired();

            entity.ToTable("Relative");
        }
    }
}