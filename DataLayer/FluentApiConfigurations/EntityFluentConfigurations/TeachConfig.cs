using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class TeachConfig : IEntityTypeConfiguration<Teach>
    {
        public void Configure(EntityTypeBuilder<Teach> entity)
        {
            entity.HasKey(t => new {t.DisCode, t.TeacherCode});
            
            // entity.Property(t => t.TeacherCode).HasDefaultValueSql();

            entity.HasOne<Teacher>().WithMany().HasForeignKey(t => t.TeacherCode).IsRequired();
            entity.HasOne<Discipline>().WithMany().HasForeignKey(t => t.DisCode).IsRequired();
        }
    }
}