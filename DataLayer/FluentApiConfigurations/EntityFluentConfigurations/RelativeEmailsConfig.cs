using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class RelativeEmailsConfig : IEntityTypeConfiguration<RelativeEmails>
    {
        public void Configure(EntityTypeBuilder<RelativeEmails> entity)
        {
            entity.HasKey(e => e.Email);

            entity.Property(e => e.Email).IsRequired().HasMaxLength(30);

            entity.HasOne<Relative>().WithMany().HasForeignKey(e => e.RelCode).IsRequired();

            entity.ToTable("RelativeEmails");
        }
    }
}