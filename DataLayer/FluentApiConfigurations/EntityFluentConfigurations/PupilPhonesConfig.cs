using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class PupilPhonesConfig : IEntityTypeConfiguration<PupilPhones>
    {
        public void Configure(EntityTypeBuilder<PupilPhones> entity)
        {
            entity.HasKey(e => e.PhoneNum);
            
            entity.Property(e => e.PhoneNum).IsRequired().HasMaxLength(10);

            entity.HasOne<Pupil>().WithMany().HasForeignKey(e => e.PupCode).IsRequired();

            entity.ToTable("PupilPhones");
        }
    }
}