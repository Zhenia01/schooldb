using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class FamilyConfig : IEntityTypeConfiguration<Family>
    {
        public void Configure(EntityTypeBuilder<Family> entity)
        {
            entity.HasKey(f => new {f.PupCode, f.RelCode});
            
            entity.Property(f => f.Type).IsRequired().HasMaxLength(10);

            entity.HasOne<Pupil>().WithMany().HasForeignKey(f => f.PupCode).IsRequired();
            entity.HasOne<Relative>().WithMany().HasForeignKey(f => f.RelCode).IsRequired();
        }
    }
}