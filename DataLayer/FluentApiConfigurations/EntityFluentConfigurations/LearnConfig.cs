using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.EntityFluentConfigurations
{
    public class LearnConfig : IEntityTypeConfiguration<Learn>
    {
        public void Configure(EntityTypeBuilder<Learn> entity)
        {
            entity.HasKey(l => new {l.PupCode, l.DisCode});

            entity.Property(l => l.SemesterGrade).HasColumnType("SMALLINT");

            entity.HasOne<Pupil>().WithMany().HasForeignKey(p => p.PupCode).IsRequired();
            entity.HasOne<Discipline>().WithMany().HasForeignKey(p => p.DisCode).IsRequired();
        }
    }
}