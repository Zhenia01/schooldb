using DataLayer.QueryObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.KeylessEntityFluentConfigurations
{
    public class FamilyPupilsConfig : IEntityTypeConfiguration<FamilyPupils>
    {
        public void Configure(EntityTypeBuilder<FamilyPupils> entity)
        {
            entity.HasNoKey().ToView(null);
        }
    }
}