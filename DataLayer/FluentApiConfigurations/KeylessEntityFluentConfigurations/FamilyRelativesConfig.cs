using DataLayer.QueryObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.KeylessEntityFluentConfigurations
{
    public class FamilyRelativesConfig : IEntityTypeConfiguration<FamilyRelatives>
    {
        public void Configure(EntityTypeBuilder<FamilyRelatives> entity)
        {
            entity.HasNoKey().ToView(null);
        }
    }
}