using DataLayer.QueryObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.KeylessEntityFluentConfigurations
{
    public class PupilCountOfSkipsConfig : IEntityTypeConfiguration<PupilCountOfSkips>
    {
        public void Configure(EntityTypeBuilder<PupilCountOfSkips> entity)
        {
            entity.HasNoKey().ToView(null);
        }
    }
}