using DataLayer.QueryObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.KeylessEntityFluentConfigurations
{
    public class EmailsConfig : IEntityTypeConfiguration<Emails>
    {
        public void Configure(EntityTypeBuilder<Emails> entity)
        {
            entity.HasNoKey().ToView(null);
        }
    }
}