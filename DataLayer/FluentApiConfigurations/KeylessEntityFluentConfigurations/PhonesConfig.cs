using DataLayer.QueryObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.KeylessEntityFluentConfigurations
{
    public class PhonesConfig : IEntityTypeConfiguration<Phones>
    {
        public void Configure(EntityTypeBuilder<Phones> entity)
        {
            entity.HasNoKey().ToView(null);
        }
    }
}