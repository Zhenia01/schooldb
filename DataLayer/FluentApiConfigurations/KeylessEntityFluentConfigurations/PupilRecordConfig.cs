using DataLayer.QueryObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.KeylessEntityFluentConfigurations
{
    public class PupilRecordConfig : IEntityTypeConfiguration<PupilRecord> 
    {
        public void Configure(EntityTypeBuilder<PupilRecord> entity)
        {
            entity.HasNoKey().ToView(null);
        }
    }
}