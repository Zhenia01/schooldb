using DataLayer.QueryObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataLayer.FluentApiConfigurations.KeylessEntityFluentConfigurations
{
    public class PupilCountOfMarksСonfig : IEntityTypeConfiguration<PupilCountOfMarks>
    {
        public void Configure(EntityTypeBuilder<PupilCountOfMarks> entity)
        {
            entity.HasNoKey().ToView(null);
        }
    }
}