using DataLayer.Entities;
using DataLayer.FluentApiConfigurations.EntityFluentConfigurations;
using Microsoft.EntityFrameworkCore;

namespace DataLayer
{
    public sealed class SchoolContext : DbContext
    {
        public DbSet<Pupil> Pupils { get; set; }
        public DbSet<Relative> Relatives { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Discipline> Disciplines { get; set; }

        public SchoolContext(DbContextOptions options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(PupilConfig).Assembly);
        }
    }
}