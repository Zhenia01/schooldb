using System.Security.Claims;
using Blazored.Modal;
using DataLayer;
using DataLayer.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Radzen;
using SchoolWebApp.Areas.Identity;
using SchoolWebApp.Areas.Identity.Stores;
using ServiceLayer.Discipline.Repositories;
using ServiceLayer.Family.Repositories;
using ServiceLayer.Learn.Repositories;
using ServiceLayer.Lesson.Repositories;
using ServiceLayer.Pupil.Repositories;
using ServiceLayer.Record.Repositories;
using ServiceLayer.Relative.Repositories;
using ServiceLayer.Role.Repositories;
using ServiceLayer.Subject.Repositories;
using ServiceLayer.Teach.Repositories;
using ServiceLayer.Teacher.Repositories;
using UserStore = SchoolWebApp.Areas.Identity.Stores.UserStore;

namespace SchoolWebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SchoolContext>(
                options => options.UseMySql(Configuration.GetConnectionString("SchoolConnection"),
                    b => b.MigrationsAssembly("DataLayer"))
            );

            services.AddDefaultIdentity<User>(options =>
                {
                    options.SignIn.RequireConfirmedAccount = true;
                    options.SignIn.RequireConfirmedEmail = false;

                    options.Password.RequiredLength = 6;
                    options.Password.RequireDigit = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                })
                .AddRoles<Role>()
                .AddUserStore<UserStore>()
                .AddRoleStore<RoleStore>();

            services.AddTransient<TeacherQueries>();
            services.AddTransient<TeacherCommands>();
            services.AddTransient<RelativeQueries>();
            services.AddTransient<RelativeCommands>();
            services.AddTransient<PupilQueries>();
            services.AddTransient<PupilCommands>();
            services.AddTransient<FamilyQueries>();
            services.AddTransient<FamilyCommands>();
            services.AddTransient<SubjectQueries>();
            services.AddTransient<SubjectCommands>();
            services.AddTransient<DisciplineQueries>();
            services.AddTransient<DisciplineCommands>();
            // services.AddTransient<RoleRepository>();
            services.AddTransient<TeachCommands>();
            services.AddTransient<TeachQueries>();            
            services.AddTransient<LearnCommands>();
            services.AddTransient<LearnQueries>();            
            services.AddTransient<LessonCommands>();
            services.AddTransient<LessonQueries>();         
            services.AddTransient<RecordCommands>();
            services.AddTransient<RecordQueries>();

            services.AddBlazoredModal();
            services.AddScoped<NotificationService>();

            services.AddRazorPages();
            services.AddServerSideBlazor();
            services
                .AddScoped<AuthenticationStateProvider,
                    RevalidatingIdentityAuthenticationStateProvider<IdentityUser>>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}