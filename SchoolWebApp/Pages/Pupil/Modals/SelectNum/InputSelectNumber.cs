using Microsoft.AspNetCore.Components.Forms;

namespace SchoolWebApp.Pages.Pupil.Modals.SelectNum
{
    public class InputSelectNumber<T> : InputSelect<T>
    {
        protected override bool TryParseValueFromString(string value, out T result, out string validationErrorMessage)
        {
            if (typeof(T) == typeof(int?) || typeof(T) == typeof(int))
            {
                if (typeof(T) == typeof(int?) && value == null)
                {
                    result = (T)(object)null;
                    validationErrorMessage = null;
                    return true;
                }
                if (int.TryParse(value, out var resultInt))
                {
                    result = (T)(object)resultInt;
                    validationErrorMessage = null;
                    return true;
                }
                else
                {
                    result = default;
                    validationErrorMessage = "The chosen value is not a valid number.";
                    return false;
                }
            }
            else
            {
                return base.TryParseValueFromString(value, out result, out validationErrorMessage);
            }
        }
    }
}