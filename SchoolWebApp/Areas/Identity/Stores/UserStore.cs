using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using DataLayer;
using DataLayer.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.Web.CodeGeneration.DotNet;

namespace SchoolWebApp.Areas.Identity.Stores
{
    public class UserStore : IUserPasswordStore<User>, IUserRoleStore<User>, IUserEmailStore<User>
    {
        private readonly SchoolContext _context;

        public UserStore(SchoolContext context)
        {
            _context = context;
        }

        public void Dispose()
        {
        }

        public async Task<string> GetUserIdAsync(User user, CancellationToken cancellationToken)
        {
            return await Task.FromResult(user.Login);
        }

        public async Task<string> GetUserNameAsync(User user, CancellationToken cancellationToken)
        {
            return await Task.FromResult(user.Login);
        }

        public Task SetUserNameAsync(User user, string userName, CancellationToken cancellationToken)
        {
            user.Login = userName;
            // _context.Set<User>().Update(user);
            // _context.SaveChanges();
            return Task.CompletedTask;
        }

        public Task<string> GetNormalizedUserNameAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Login.ToUpper());
        }

        public Task SetNormalizedUserNameAsync(User user, string normalizedName, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task<IdentityResult> CreateAsync(User user, CancellationToken cancellationToken)
        {
            _context.Add(user);
            _context.SaveChanges();
            return Task.FromResult(IdentityResult.Success);
        }

        public Task<IdentityResult> UpdateAsync(User user, CancellationToken cancellationToken)
        {
            _context.Set<User>().Update(user);
            _context.SaveChanges();
            return Task.FromResult(IdentityResult.Success);
        }

        public Task<IdentityResult> DeleteAsync(User user, CancellationToken cancellationToken)
        {
            _context.Set<User>().Remove(user);
            _context.SaveChanges();
            return Task.FromResult(IdentityResult.Success);
        }

        public async Task<User> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            return await _context.Set<User>().FindAsync(userId);
        }

        public async Task<User> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            return await _context.Set<User>().FirstOrDefaultAsync(p => p.Login.ToUpper().Equals(normalizedUserName),
                cancellationToken);
        }

        public Task SetPasswordHashAsync(User user, string passwordHash, CancellationToken cancellationToken)
        {
            user.Password = passwordHash;
            // _context.Set<User>().Update(user);

            return Task.CompletedTask;
        }

        public Task<string> GetPasswordHashAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Password);
        }

        public Task<bool> HasPasswordAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(!string.IsNullOrWhiteSpace(user.Password));
        }

        public async Task AddToRoleAsync(User user, string roleName, CancellationToken cancellationToken)
        {
            await _context.Set<UserRole>()
                .AddAsync(new UserRole {Login = user.Login, Name = roleName}, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
        }

        public Task RemoveFromRoleAsync(User user, string roleName, CancellationToken cancellationToken)
        {
            _context.Set<UserRole>().Remove(new UserRole {Login = user.Login, Name = roleName});
            _context.SaveChanges();
            return Task.CompletedTask;
        }

        public Task<IList<string>> GetRolesAsync(User user, CancellationToken cancellationToken)
        {
            IList<string> res = _context.Set<UserRole>().Where(ur => ur.Login == user.Login).Select(ur => ur.Name)
                .ToImmutableList();
            return Task.FromResult(res);
        }

        public async Task<bool> IsInRoleAsync(User user, string roleName, CancellationToken cancellationToken)
        {
            return await
                _context.Set<UserRole>().FindAsync(user.Login, roleName) != null;
        }

        public Task<IList<User>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            IList<User> res = _context.Set<User>().FromSqlInterpolated(
                @$" SELECT User.Login, User.Password
                    FROM User INNER JOIN UserRole ON User.Login = UserRole.Login
                    WHERE UserRole = {roleName}").ToImmutableList();
            return Task.FromResult(res);
        }

        public async Task SetEmailAsync(User user, string email, CancellationToken cancellationToken)
        {
            await SetUserNameAsync(user, email, cancellationToken);
        }

        public async Task<string> GetEmailAsync(User user, CancellationToken cancellationToken)
        {
            return await GetUserNameAsync(user, cancellationToken);
        }

        public Task<bool> GetEmailConfirmedAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(true);
        }

        public Task SetEmailConfirmedAsync(User user, bool confirmed, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task<User> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken)
        {
            return FindByNameAsync(normalizedEmail, cancellationToken);
        }

        public async Task<string> GetNormalizedEmailAsync(User user, CancellationToken cancellationToken)
        {
            return (await GetUserNameAsync(user, cancellationToken)).ToUpper();
        }

        public Task SetNormalizedEmailAsync(User user, string normalizedEmail, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}