using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Blazored.Modal;
using Blazored.Modal.Services;
using Microsoft.AspNetCore.Components;

namespace SchoolWebApp
{
    public static class Extensions
    {
        public static async Task ShowModalWithActionOnClose<TComponent>(this IModalService modalService, string title,
            Action action, ModalParameters parameters) where TComponent : ComponentBase
        {
            var result = await modalService.Show<TComponent>(title, parameters).Result;

            if (!result.Cancelled)
            {
                action.Invoke();
            }
        }
        
        public static async Task ShowModalWithActionOnClose<TComponent>(this IModalService modalService, string title,
            Action action, params (string, object)[] parameters) where TComponent : ComponentBase
        {
            var modalParams = new ModalParameters();

            foreach ((string name, object value) in parameters)
            {
                modalParams.Add(name, value);
            }

            var result = await modalService.Show<TComponent>(title, modalParams).Result;

            if (!result.Cancelled)
            {
                action.Invoke();
            }
        }
    }
}