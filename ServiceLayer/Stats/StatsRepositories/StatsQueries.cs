using System.Collections.Generic;
using System.Collections.Immutable;
using DataLayer;
using DataLayer.QueryObjects;
using Microsoft.EntityFrameworkCore;

namespace ServiceLayer.Stats.StatsRepositories
{
    public class StatsQueries
    {
        private readonly SchoolContext _context;

        public StatsQueries(SchoolContext context)
        {
            _context = context;
        }

        public IEnumerable<PupilCountOfMarks> CountOfMarks(int pupCode)
        {
            return _context.Set<PupilCountOfMarks>().FromSqlInterpolated(
                $@" SELECT Grade, COUNT(*) AS `Count`
                    FROM Record
                    WHERE PupCode = {pupCode}
                    GROUP BY Grade"
            ).AsNoTracking().ToImmutableList();
        }
        
        
    }
}