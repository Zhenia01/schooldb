using System.ComponentModel.DataAnnotations;

namespace ServiceLayer.Relative.Dto
{
    public class RelativeDto
    {
        public int RelCode { get; set; }
        
        [MaxLength(25, ErrorMessage = "Масимальна довжина імені = 25")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Ім'я - обов'язкове поле")]
        public string LastName { get; set; }
        
        [MaxLength(25, ErrorMessage = "Масимальна довжина прізвища = 25")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Прізвище - обов'язкове поле")]
        public string FirstName { get; set; }
        
        [MaxLength(25, ErrorMessage = "Масимальна довжина по-батькові = 25")]
        public string MiddleName { get; set; }
    }
}