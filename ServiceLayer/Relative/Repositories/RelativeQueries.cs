using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using DataLayer;
using DataLayer.QueryObjects;
using Microsoft.EntityFrameworkCore;
using ServiceLayer.Common.EmailsPhones.Dto;
using ServiceLayer.Common.EmailsPhones.Interfaces;
using ServiceLayer.Relative.Dto;

namespace ServiceLayer.Relative.Repositories
{
    public class RelativeQueries : IPhonesQueries, IEmailsQueries
    {
        private readonly SchoolContext _context;

        public RelativeQueries(SchoolContext context)
        {
            _context = context;
        }
        
        public IEnumerable<RelativeDto> GetRelatives()
        {
            return _context.Relatives.FromSqlRaw(
                "SELECT *" +
                "FROM Relative").AsNoTracking().Select(t => new RelativeDto
            {
                RelCode = t.RelCode,
                FirstName = t.FirstName,
                LastName = t.LastName,
                MiddleName = t.MiddleName,
            });
        }
        
        public IEnumerable<EmailDto> GetEmails(int code)
        {
            return _context.Set<Emails>().FromSqlInterpolated(
                $@"SELECT Email
                FROM RelativeEmails
                WHERE RelCode={code}").AsNoTracking().Select(email => new EmailDto { Email = email.Email }).ToImmutableList();
        }
        public IEnumerable<PhoneDto> GetPhones(int code)
        {
            return _context.Set<Phones>().FromSqlInterpolated(
                $@"SELECT PhoneNum
                FROM RelativePhones
                WHERE RelCode={code}").AsNoTracking().Select(phone => new PhoneDto{Phone = phone.PhoneNum}).ToImmutableList();
        }

        public RelativeDto GetRelative(int code)
        {
            var relative = _context.Relatives.FromSqlInterpolated(
                $@" SELECT *
                    FROM Relative
                    WHERE RelCode = {code}").AsNoTracking().First(); 
            return new RelativeDto
            {
                RelCode = relative.RelCode,
                FirstName = relative.FirstName,
                LastName = relative.LastName,
                MiddleName = relative.MiddleName,
            };
        }
        
        public RelativeDto GetRelative(string login)
        {
            var relative = _context.Relatives.FromSqlInterpolated(
                $@" SELECT *
                    FROM Relative
                    WHERE Login = {login}").AsNoTracking().First(); 
            return new RelativeDto
            {
                RelCode = relative.RelCode,
                FirstName = relative.FirstName,
                LastName = relative.LastName,
                MiddleName = relative.MiddleName,
            };
        }

        public string GetLogin(int code)
        {
            return _context.Set<UserLogin>().FromSqlInterpolated(
                $@" SELECT Login
                    FROM Relative
                    WHERE RelCode={code}").AsNoTracking().First().Login;
        }
    }
}