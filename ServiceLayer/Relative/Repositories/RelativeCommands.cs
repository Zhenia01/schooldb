using System;
using DataLayer;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using ServiceLayer.Common.EmailsPhones.Dto;
using ServiceLayer.Common.EmailsPhones.Interfaces;
using ServiceLayer.Relative.Dto;

namespace ServiceLayer.Relative.Repositories
{
    public class RelativeCommands : IEmailsCommands, IPhonesCommands
    {
        private readonly SchoolContext _context;

        public RelativeCommands(SchoolContext context)
        {
            _context = context;
        }

        public void AddRelative(RelativeDto dto, string login)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" INSERT INTO Relative (LastName, FirstName, MiddleName, Login)
                    VALUES ({dto.LastName}, {dto.FirstName}, {dto.MiddleName}, {login})");
        }

        public void DeleteRelative(int code)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" DELETE FROM Relative
                    WHERE RelCode = {code}");
        }

        public void UpdateRelative(RelativeDto dto)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" UPDATE Relative 
                    SET LastName={dto.LastName}, 
                        FirstName={dto.FirstName}, 
                        MiddleName={dto.MiddleName}  
                    WHERE RelCode={dto.RelCode}");
        }

        public void AddPhone(int code, PhoneDto phone)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" INSERT INTO RelativePhones
                    VALUES ({phone.Phone}, {code})");
        }

        public void AddEmail(int code, EmailDto email)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" INSERT INTO RelativeEmails
                        VALUES ({email.Email}, {code})");
        }

        public void DeletePhone(PhoneDto phone)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$"DELETE FROM RelativePhones
                   WHERE PhoneNum = {phone.Phone}");
        }

        public void DeleteEmail(EmailDto email)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$"DELETE FROM RelativeEmails
                   WHERE Email = {email.Email}");
        }

        public void UpdatePhone(PhoneDto oldPhone, PhoneDto newPhone)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" UPDATE RelativePhones
                    SET PhoneNum={newPhone.Phone}
                    WHERE PhoneNum={oldPhone.Phone}");
        }

        public void UpdateEmail(EmailDto oldEmail, EmailDto newEmail)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" UPDATE RelativeEmails
                    SET Email={newEmail.Email}
                    WHERE Email={oldEmail.Email}");
        }
    }
}