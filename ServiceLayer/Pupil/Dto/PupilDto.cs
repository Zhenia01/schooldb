using System;
using System.ComponentModel.DataAnnotations;

namespace ServiceLayer.Pupil.Dto
{
    public class PupilDto
    {
        public int PupCode { get; set; }
        
        [MaxLength(25, ErrorMessage = "Масимальна довжина імені = 25")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Ім'я - обов'язкове поле")]
        public string LastName { get; set; }
        
        [MaxLength(25, ErrorMessage = "Масимальна довжина прізвища = 25")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Прізвище - обов'язкове поле")]
        public string FirstName { get; set; }
        
        [MaxLength(25, ErrorMessage = "Масимальна довжина по-батькові = 25")]
        public string MiddleName { get; set; }
        
        [Required(ErrorMessage = "Дата народження - обов'язкове поле")]
        [BirthDate(ErrorMessage = "Введіть коректну дату народження")]
        public DateTime BirthDate { get; set; }
        
        [Required(ErrorMessage = "Адреса - обов'язкове поле")]
        [MaxLength(25, ErrorMessage = "Масимальна довжина адреси = 50")]
        public string Address { get; set; }
    }
}