using System;
using System.ComponentModel.DataAnnotations;

namespace ServiceLayer.Pupil.Dto
{
    public class BirthDateAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value is DateTime date)
            {
                int age = CalcAge(date);

                return age > 5 && age < 20;
            }

            return false;
        }

        private static int CalcAge(DateTime birthDate)
        {
            var today = DateTime.Today;
            var age = today.Year - birthDate.Year;
            if (birthDate > today.AddYears(-age)) return --age;
            return age;
        }
    }
}