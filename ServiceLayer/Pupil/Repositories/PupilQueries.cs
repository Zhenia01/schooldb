using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using DataLayer;
using DataLayer.QueryObjects;
using Microsoft.EntityFrameworkCore;
using ServiceLayer.Common.EmailsPhones.Dto;
using ServiceLayer.Common.EmailsPhones.Interfaces;
using ServiceLayer.Pupil.Dto;

namespace ServiceLayer.Pupil.Repositories
{
    public class PupilQueries : IPhonesQueries, IEmailsQueries
    {
        private readonly SchoolContext _context;

        public PupilQueries(SchoolContext context)
        {
            _context = context;
        }

        public IEnumerable<PupilDto> GetPupils()
        {
            return _context.Pupils.FromSqlRaw(
                "SELECT PupCode, LastName, FirstName, MiddleName, Address, BirthDate " +
                "FROM Pupil").AsNoTracking().Select(t => new PupilDto
            {
                PupCode = t.PupCode,
                FirstName = t.FirstName,
                LastName = t.LastName,
                MiddleName = t.MiddleName,
                Address = t.Address,
                BirthDate = t.BirthDate
            }).ToImmutableList();
        }

        public PupilDto GetPupil(int pupCode)
        {
            DataLayer.Entities.Pupil pupil = _context.Pupils.FromSqlInterpolated(
                $@" SELECT *
                    FROM Pupil
                    WHERE PupCode = {pupCode}").AsNoTracking().First();
            return new PupilDto
            {
                PupCode = pupil.PupCode,
                FirstName = pupil.FirstName,
                LastName = pupil.LastName,
                MiddleName = pupil.MiddleName,
                Address = pupil.Address,
                BirthDate = pupil.BirthDate
            };
        }
        
        public PupilDto GetPupil(string login)
        {
            DataLayer.Entities.Pupil pupil = _context.Pupils.FromSqlInterpolated(
                $@" SELECT *
                    FROM Pupil
                    WHERE Login = {login}").AsNoTracking().First();
            return new PupilDto
            {
                PupCode = pupil.PupCode,
                FirstName = pupil.FirstName,
                LastName = pupil.LastName,
                MiddleName = pupil.MiddleName,
                Address = pupil.Address,
                BirthDate = pupil.BirthDate
            };
        }

        public IEnumerable<EmailDto> GetEmails(int code)
        {
            return _context.Set<Emails>().FromSqlInterpolated(
                    $@" SELECT Email
                        FROM PupilEmails
                        WHERE PupCode={code}").AsNoTracking().Select(email => new EmailDto {Email = email.Email})
                .ToImmutableList();
        }

        public IEnumerable<PhoneDto> GetPhones(int code)
        {
            return _context.Set<Phones>().FromSqlInterpolated(
                    $@" SELECT PhoneNum
                        FROM PupilPhones
                        WHERE PupCode={code}").AsNoTracking().Select(phone => new PhoneDto {Phone = phone.PhoneNum})
                .ToImmutableList();
        }

        public string GetLogin(int code)
        {
            return _context.Set<UserLogin>().FromSqlInterpolated(
                $@" SELECT Login
                    FROM Pupil
                    WHERE PupCode={code}").AsNoTracking().First().Login;
        }
    }
}