using System;
using DataLayer;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using ServiceLayer.Common.EmailsPhones.Dto;
using ServiceLayer.Common.EmailsPhones.Interfaces;
using ServiceLayer.Pupil.Dto;

namespace ServiceLayer.Pupil.Repositories
{
    public class PupilCommands : IEmailsCommands, IPhonesCommands
    {
        private readonly SchoolContext _context;

        public PupilCommands(SchoolContext context)
        {
            _context = context;
        }

        public void AddPupil(PupilDto pupil, string login)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" INSERT INTO Pupil (LastName, FirstName, MiddleName, BirthDate, Address, Login)
                    VALUES ({pupil.LastName}, {pupil.FirstName}, {pupil.MiddleName}, {pupil.BirthDate}, {pupil.Address}, {login})");
        }

        public void DeletePupil(int code)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" DELETE FROM Pupil
                    WHERE PupCode = {code}");
        }

        public void UpdatePupil(PupilDto dto)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" UPDATE Pupil 
                    SET LastName={dto.LastName}, 
                        FirstName={dto.FirstName}, 
                        MiddleName={dto.MiddleName},     
                        Address={dto.Address},
                        BirthDate={dto.BirthDate}    
                    WHERE PupCode={dto.PupCode}");
        }

        public void UpdateLogin(int code, string username)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" UPDATE Pupil 
                    SET Login = {username}    
                    WHERE PupCode={code}");
        }

        public void AddPhone(int code, PhoneDto phone)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" INSERT INTO PupilPhones
                    VALUES ({phone.Phone}, {code})");
        }

        public void AddEmail(int code, EmailDto email)
        {
            _context.Database.ExecuteSqlInterpolated(
                    @$" INSERT INTO PupilEmails
                        VALUES ({email.Email}, {code})");
                _context.SaveChanges();
            }

        public void DeletePhone(PhoneDto phone)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$"DELETE FROM PupilPhones
                   WHERE PhoneNum = {phone.Phone}");
        }

        public void DeleteEmail(EmailDto email)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$"DELETE FROM PupilEmails
                   WHERE Email = {email.Email}");
        }

        public void UpdatePhone(PhoneDto oldPhone, PhoneDto newPhone)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" UPDATE PupilPhones
                    SET PhoneNum={newPhone.Phone}
                    WHERE PhoneNum={oldPhone.Phone}");
        }

        public void UpdateEmail(EmailDto oldEmail, EmailDto newEmail)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" UPDATE PupilEmails
                    SET Email={newEmail.Email}
                    WHERE Email={oldEmail.Email}");
        }
    }
}