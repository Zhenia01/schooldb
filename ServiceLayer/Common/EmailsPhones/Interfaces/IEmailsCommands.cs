using ServiceLayer.Common.EmailsPhones.Dto;

namespace ServiceLayer.Common.EmailsPhones.Interfaces
{
    public interface IEmailsCommands
    {
        void AddEmail(int code, EmailDto email);
        void DeleteEmail(EmailDto email);
        void UpdateEmail(EmailDto oldEmail, EmailDto newEmail);
    }
}