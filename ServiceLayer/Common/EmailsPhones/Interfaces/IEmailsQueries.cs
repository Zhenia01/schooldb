using System.Collections.Generic;
using System.Collections.Immutable;
using ServiceLayer.Common.EmailsPhones.Dto;

namespace ServiceLayer.Common.EmailsPhones.Interfaces
{
    public interface IEmailsQueries
    {
        IEnumerable<EmailDto> GetEmails(int code);
    }
}