using ServiceLayer.Common.EmailsPhones.Dto;

namespace ServiceLayer.Common.EmailsPhones.Interfaces
{
    public interface IPhonesCommands
    {
        void AddPhone(int code, PhoneDto phone);
        void DeletePhone(PhoneDto phone);
        void UpdatePhone(PhoneDto oldPhone, PhoneDto newPhone);
    }
}