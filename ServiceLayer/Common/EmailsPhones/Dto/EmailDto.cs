using System.ComponentModel.DataAnnotations;

namespace ServiceLayer.Common.EmailsPhones.Dto
{
    public class EmailDto
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Імейл - обов'язкове поле")]
        [EmailAddress(ErrorMessage = "Неправильний формат імейлу")]
        [MaxLength(20, ErrorMessage = "Максимальна довжина = 30")]
        public string Email { get; set; }
    }
}