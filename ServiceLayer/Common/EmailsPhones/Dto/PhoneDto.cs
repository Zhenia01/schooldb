using System.ComponentModel.DataAnnotations;

namespace ServiceLayer.Common.EmailsPhones.Dto
{
    public class PhoneDto
    {
        [Required(ErrorMessage = "Номер телефону - обов'язкове поле")]
        [RegularExpression("[0-9]{10}", ErrorMessage = "Формат номеру - 10 цифр")]
        public string Phone { get; set; }
    }
}