using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using DataLayer;
using Microsoft.EntityFrameworkCore;
using ServiceLayer.Discipline.Dto;
using ServiceLayer.Pupil.Dto;

namespace ServiceLayer.Learn.Repositories
{
    public class LearnQueries
    {
        private readonly SchoolContext _context;

        public LearnQueries(SchoolContext context)
        {
            _context = context;
        }

        public IEnumerable<PupilDto> GetLearnPupils(int disCode)
        {
            return _context.Pupils.FromSqlInterpolated(
                $@" SELECT *
                    FROM Pupil
                    WHERE PupCode IN (
                        SELECT PupCode
                        FROM Learn
                        WHERE DisCode = {disCode}               
                    ) ").AsNoTracking().ToImmutableList().Select(p => new PupilDto
            {
                PupCode = p.PupCode,
                FirstName = p.FirstName,
                LastName = p.LastName,
                MiddleName = p.MiddleName
            });
        }
        
        public IEnumerable<DisciplineDto> GetLearnDisciplines(int pupCode)
        {
            return _context.Disciplines.FromSqlInterpolated(
                $@" SELECT *
                    FROM Discipline
                    WHERE DisCode IN (
                        SELECT DisCode
                        FROM Learn
                        WHERE PupCode = {pupCode}               
                    ) ").AsNoTracking().ToImmutableList().Select(d => new DisciplineDto
            {
                DisCode = d.DisCode,
                SubjName = d.SubjName,
                DateYear = d.DateYear,
                AcademYear = d.AcademYear,
                Semester = d.Semester,
                Group = d.Group
            });
        }
        
        public IEnumerable<PupilDto> GetNonLearnPupils(int disCode)
        {
            return _context.Pupils.FromSqlInterpolated(
                $@" SELECT *
                    FROM Pupil AS P
                    WHERE NOT EXISTS (
                        SELECT *
                        FROM Learn
                        WHERE DisCode = {disCode} AND PupCode = P.PupCode
                    )"
            ).AsNoTracking().Select(p => new PupilDto
            {
                PupCode = p.PupCode,
                FirstName = p.FirstName,
                LastName = p.LastName,
                MiddleName = p.MiddleName
            });
        }
    }
}