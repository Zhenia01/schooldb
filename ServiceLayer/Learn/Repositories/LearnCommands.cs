using DataLayer;
using Microsoft.EntityFrameworkCore;
using ServiceLayer.Learn.Dto;

namespace ServiceLayer.Learn.Repositories
{
    public class LearnCommands
    {
        private readonly SchoolContext _context;

        public LearnCommands(SchoolContext context)
        {
            _context = context;
        }

        public void CreateLearn(LearnDto dto)
        {
            _context.Database.ExecuteSqlInterpolated(
                $@" INSERT INTO Learn(PupCode, DisCode)
                    VALUES ({dto.PupCode}, {dto.DisCode}) ");
        }

        public void DeleteLearn(LearnDto dto)
        {
            _context.Database.ExecuteSqlInterpolated(
                $@" DELETE FROM Learn
                    WHERE PupCode = {dto.PupCode} AND DisCode = {dto.DisCode}");
        }
    }
}