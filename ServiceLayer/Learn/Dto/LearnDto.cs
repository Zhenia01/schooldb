using System.ComponentModel.DataAnnotations;

namespace ServiceLayer.Learn.Dto
{
    public class LearnDto
    {
        [Required(ErrorMessage = "Учень - обов'язкове поле")]
        public int PupCode { get; set; }
        
        [Required(ErrorMessage = "Дисципліна - обов'язкове поле")]
        public int DisCode { get; set; }
    }
}