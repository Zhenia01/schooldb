using System.ComponentModel.DataAnnotations;

namespace ServiceLayer.Teach.Dto
{
    public class TeachDto
    {
        [Required(ErrorMessage = "Дисципліна - обов'язкове поле")]
        public int DisCode { get; set; }
        
        [Required(ErrorMessage = "Вчитель - обов'язкове поле")]
        public int TeacherCode { get; set; }
    }
}