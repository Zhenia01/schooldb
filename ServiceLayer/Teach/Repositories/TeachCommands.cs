using DataLayer;
using Microsoft.EntityFrameworkCore;

namespace ServiceLayer.Teach.Repositories
{
    public class TeachCommands
    {
        private readonly SchoolContext _context;

        public TeachCommands(SchoolContext context)
        {
            _context = context;
        }

        public void CreateTeach(int disCode, int teacherCode)
        {
            _context.Database.ExecuteSqlInterpolated(
                $@" INSERT INTO Teach(DisCode, TeacherCode)
                    VALUES ({disCode}, {teacherCode}) "
            );
        }

        public void UpdateTeach(int disCode, int oldTeacherCode, int newTeacherCode)
        {
            _context.Database.ExecuteSqlInterpolated(
                $@" UPDATE Teach
                    SET TeacherCode = {newTeacherCode} 
                    WHERE DisCode = {disCode} AND TeacherCode = {oldTeacherCode} "
            );
        }
    }
}