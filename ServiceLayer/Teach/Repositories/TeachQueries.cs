using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using DataLayer;
using Microsoft.EntityFrameworkCore;
using ServiceLayer.Discipline.Dto;
using ServiceLayer.Teacher.Dto;

namespace ServiceLayer.Teach.Repositories
{
    public class TeachQueries
    {
        private readonly SchoolContext _context;

        public TeachQueries(SchoolContext context)
        {
            _context = context;
        }

        public TeacherDto GetTeacherOfDiscipline(int disCode)
        {
            var teacher = _context.Teachers.FromSqlInterpolated(
                $@" SELECT Teacher.*
                    FROM Teach INNER JOIN Teacher 
                        ON Teach.TeacherCode = Teacher.TeacherCode
                    WHERE Teach.DisCode = {disCode} ").AsNoTracking().First();

            return new TeacherDto
            {
                FirstName = teacher.FirstName,
                LastName = teacher.LastName,
                MiddleName = teacher.MiddleName,
                TeacherCode = teacher.TeacherCode
            };
        }
        
        public IEnumerable<TeacherDto> GetNonTeachersOfDiscipline(int disCode)
        {
            return _context.Teachers.FromSqlInterpolated(
                $@" SELECT *
                    FROM Teacher AS T
                    WHERE NOT EXISTS (
                        SELECT *
                        FROM Teach
                        WHERE DisCode = {disCode} AND TeacherCode = T.TeacherCode
                    ) ").AsNoTracking().ToImmutableList().Select(t => new TeacherDto
            {
                FirstName = t.FirstName,
                LastName = t.LastName,
                MiddleName = t.MiddleName,
                TeacherCode = t.TeacherCode
            });
        }

        public IEnumerable<DisciplineDto> GetTeachersDisciplines(int teacherCode)
        {
            return _context.Disciplines.FromSqlInterpolated(
                    $@" SELECT *
                        FROM Discipline
                        WHERE DisCode IN (
                            SELECT DisCode
                            FROM Teach
                            WHERE TeacherCode = {teacherCode}
                        )").AsNoTracking().ToImmutableList()
                .Select(d => new DisciplineDto
                {
                    DisCode = d.DisCode,
                    SubjName = d.SubjName,
                    Group = d.Group,
                    Semester = d.Semester,
                    AcademYear = d.AcademYear,
                    DateYear = d.DateYear
                });
        }
    }
}