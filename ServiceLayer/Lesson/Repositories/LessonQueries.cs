using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using DataLayer;
using DataLayer.FluentApiConfigurations.EntityFluentConfigurations;
using Microsoft.EntityFrameworkCore;
using ServiceLayer.Lesson.Dto;

namespace ServiceLayer.Lesson.Repositories
{
    public class LessonQueries
    {
        private readonly SchoolContext _context;

        public LessonQueries(SchoolContext context)
        {
            _context = context;
        }

        public IEnumerable<LessonDto> GetDisciplinesLessons(int disCode)
        {
            return _context.Set<DataLayer.Entities.Lesson>().FromSqlInterpolated(
                $@" SELECT *
                    FROM Lesson
                    WHERE DisCode = {disCode}").AsNoTracking().ToImmutableList().Select(l => new LessonDto
            {
                LesCode = l.LesCode,
                DisCode = l.DisCode,
                TeacherCode = l.TeacherCode,
                Num = l.Num,
                LessonDate = l.LessonDate,
                LessonType = LessonConfig.EnumToString(l.LessonType),
                Theme = l.Theme,
                HomeTask = l.HomeTask 
            });
        }

        public LessonDto GetLesson(int code)
        {
            var l = _context.Set<DataLayer.Entities.Lesson>().FromSqlInterpolated(
                $@" SELECT *
                    FROM Lesson
                    WHERE LesCode = {code}").AsNoTracking().ToImmutableList().First(); 
            
            return new LessonDto
            {
                LesCode = l.LesCode,
                DisCode = l.DisCode,
                TeacherCode = l.TeacherCode,
                Num = l.Num,
                LessonDate = l.LessonDate,
                LessonType = LessonConfig.EnumToString(l.LessonType),
                Theme = l.Theme,
                HomeTask = l.HomeTask 
            };
        }
    }
}