using System;
using System.Xml;
using DataLayer;
using Microsoft.EntityFrameworkCore;
using ServiceLayer.Lesson.Dto;

namespace ServiceLayer.Lesson.Repositories
{
    public class LessonCommands
    {
        private readonly SchoolContext _context;

        public LessonCommands(SchoolContext context)
        {
            _context = context;
        }

        public void CreateLesson(LessonDto dto)
        {
            _context.Database.ExecuteSqlInterpolated(
                $@" INSERT INTO Lesson(DisCode, TeacherCode, Num, LessonDate, LessonType, Theme, HomeTask)
                    VALUES ({dto.DisCode}, {dto.TeacherCode}, {dto.Num}, {dto.LessonDate}, {dto.LessonType}, {dto.Theme}, {dto.HomeTask})");
        }

        public void UpdateLesson(LessonDto dto)
        {
            _context.Database.ExecuteSqlInterpolated(
                $@" UPDATE Lesson
                    SET DisCode = {dto.DisCode}, 
                        TeacherCode = {dto.TeacherCode}, 
                        Num = {dto.Num}, 
                        LessonDate = {dto.LessonDate}, 
                        LessonType = {dto.LessonType}, 
                        Theme = {dto.Theme},
                        HomeTask = {dto.HomeTask}
                    WHERE LesCode = {dto.LesCode}");
        }
    }
}