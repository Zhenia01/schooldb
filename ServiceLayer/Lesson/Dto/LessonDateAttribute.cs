using System;
using System.ComponentModel.DataAnnotations;

namespace ServiceLayer.Lesson.Dto
{
    public class LessonDateAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value is DateTime date)
            {
                return date < (DateTime.Now + TimeSpan.FromDays(365)) 
                       && date > (DateTime.Now - TimeSpan.FromDays(365*2)) ;
            }

            return false;
        }
    }
}