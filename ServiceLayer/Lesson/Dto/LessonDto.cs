using System;
using System.ComponentModel.DataAnnotations;
using DataLayer.Entities;

namespace ServiceLayer.Lesson.Dto
{
    public class LessonDto
    {
        public int LesCode { get; set; }
        public int DisCode { get; set; }
        public int? TeacherCode { get; set; }
        [Required(ErrorMessage = "Порядковий номер - обов'язкове поле")]
        [Range(1, 9, ErrorMessage = "Порядковий номер - число від 1 до 9")]
        public int Num { get; set; }
        [Required(ErrorMessage = "Дата - обов'язкове поле")]
        [LessonDate(ErrorMessage = "Введіть коректну дату")]
        public DateTime LessonDate { get; set; }
        
        [Required(AllowEmptyStrings = false, ErrorMessage = "Тип - обов'язкове поле")]
        [MaxLength(15, ErrorMessage = "Максимальна довжина типу = 15")]
        public string LessonType { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Тема - обов'язкове поле")]
        [MaxLength(100, ErrorMessage = "Максимальна довжина теми = 25")]
        public string Theme { get; set; }
        [MaxLength(100, ErrorMessage = "Максимальна довжина д/з = 100")]
        public string HomeTask { get; set; }
    }
}