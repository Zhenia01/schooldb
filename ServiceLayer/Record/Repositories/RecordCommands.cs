using System;
using System.Collections.Generic;
using System.Linq;
using DataLayer;
using Microsoft.EntityFrameworkCore;
using ServiceLayer.Record.Dto;

namespace ServiceLayer.Record.Repositories
{
    public class RecordCommands
    {
        private readonly SchoolContext _context;

        public RecordCommands(SchoolContext context)
        {
            _context = context;
        }

        public void CreateRecords(IEnumerable<RecordDto> records)
        {
            foreach (var record in records)
            {
                _context.Database.ExecuteSqlInterpolated(
                    $@" INSERT INTO Record(PupCode, LesCode, Attendance, Grade, Comment)
                    VALUES({record.PupCode}, {record.LesCode}, {Convert.ToInt32(record.Attendance)}, {record.Grade}, {record.Comment}) "
                );
            }
        }

        public void UpdateRecord(RecordDto record)
        {
            _context.Database.ExecuteSqlInterpolated(
                $@" UPDATE Record
                    SET Attendance = {record.Attendance},
                        Grade = {record.Grade},
                        Comment = {record.Comment}
                    WHERE RecordCode = {record.RecordCode}");
        }
    }
}