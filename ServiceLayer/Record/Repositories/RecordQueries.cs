using System.Collections.Generic;
using DataLayer;
using DataLayer.FluentApiConfigurations.KeylessEntityFluentConfigurations;
using DataLayer.QueryObjects;
using Microsoft.EntityFrameworkCore;

namespace ServiceLayer.Record.Repositories
{
    public class RecordQueries
    {
        private readonly SchoolContext _context;

        public RecordQueries(SchoolContext context)
        {
            _context = context;
        }

        public IEnumerable<PupilRecord> GetPupilRecordsOfLesson(int code)
        {
            return _context.Set<PupilRecord>().FromSqlInterpolated(
                $@" SELECT P.PupCode, R.RecordCode, P.LastName, P.FirstName, P.MiddleName,
                        R.Attendance, R.Grade, R.Comment
                    FROM Pupil AS P INNER JOIN Record AS R 
                        ON P.PupCode = R.PupCode
                    WHERE R.LesCode = {code}");
        } 
    }
}