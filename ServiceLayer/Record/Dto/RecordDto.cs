using System.ComponentModel.DataAnnotations;

namespace ServiceLayer.Record.Dto
{
    public class RecordDto
    {
        public int RecordCode { get; set; }
        public int PupCode { get; set; }
        public int LesCode { get; set; }
        [Required(ErrorMessage = "Відвідування - обов'язкове поле")]
        public bool Attendance { get; set; }
        [Range(1, 12, ErrorMessage = "Оцінка - число від 1 до 12")]
        public int? Grade { get; set; }
        [MaxLength(100, ErrorMessage = "Максимальна довжина коментаря = 100")]
        public string Comment { get; set; }
    }
}