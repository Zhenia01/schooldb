using DataLayer;
using Microsoft.EntityFrameworkCore;

namespace ServiceLayer.Role.Repositories
{
    public class RoleRepository
    {
        private readonly SchoolContext _context;

        public RoleRepository(SchoolContext context)
        {
            _context = context;
        }

        public void AddRole(string name)
        {
            _context.Database.ExecuteSqlInterpolated(
                $@" INSERT INTO Role 
                    VALUES ({name})");
        }
    }
}