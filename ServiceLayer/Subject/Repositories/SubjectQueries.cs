using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using DataLayer;
using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using ServiceLayer.Subject.Dto;

namespace ServiceLayer.Subject.Repositories
{
    public class SubjectQueries
    {
        private readonly SchoolContext _context;

        public SubjectQueries(SchoolContext context)
        {
            _context = context;
        }

        public IEnumerable<SubjectDto> GetSubjects()
        {
            return _context.Set<DataLayer.Entities.Subject>().FromSqlRaw(
                "SELECT SubjName " +
                "FROM Subject " +
                "ORDER BY SubjName"
            ).AsNoTracking().Select(s => new SubjectDto {SubjName = s.SubjName}).ToImmutableList();
        }
    }
}