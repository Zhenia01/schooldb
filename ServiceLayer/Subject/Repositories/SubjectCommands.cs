using System.Security.Cryptography.X509Certificates;
using DataLayer;
using Microsoft.EntityFrameworkCore;
using ServiceLayer.Subject.Dto;

namespace ServiceLayer.Subject.Repositories
{
    public class SubjectCommands
    {
        private readonly SchoolContext _context;

        public SubjectCommands(SchoolContext context)
        {
            _context = context;
        }

        public void CreateSubject(SubjectDto subj)
        {
            _context.Database.ExecuteSqlInterpolated(
                $@" INSERT INTO Subject
                    VALUES ({subj.SubjName})");
        }
        
        public void DeleteSubject(SubjectDto subj)
        {
            _context.Database.ExecuteSqlInterpolated(
                $@" DELETE FROM Subject
                    WHERE SubjName = {subj.SubjName}");
        }
    }
}