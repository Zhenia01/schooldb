using System.ComponentModel.DataAnnotations;

namespace ServiceLayer.Subject.Dto
{
    public class SubjectDto
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Назва - обов'язкове поле")]
        [MaxLength(20, ErrorMessage = "Maксимальна довжина назви = 20")]
        public string SubjName { get; set; }
    }
}