using System;
using System.ComponentModel.DataAnnotations;
using DataLayer;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using ServiceLayer.Common.EmailsPhones;
using ServiceLayer.Common.EmailsPhones.Dto;
using ServiceLayer.Common.EmailsPhones.Interfaces;
using ServiceLayer.Teacher.Dto;

namespace ServiceLayer.Teacher.Repositories
{
    public class TeacherCommands : IEmailsCommands, IPhonesCommands
    {
        private readonly SchoolContext _context;

        public TeacherCommands(SchoolContext context)
        {
            _context = context;
        }

        public void AddTeacher(TeacherDto dto, string login)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" INSERT INTO Teacher (LastName, FirstName, MiddleName, Login)
                    VALUES ({dto.LastName}, {dto.FirstName}, {dto.MiddleName}, {login})");
        }

        public void DeleteTeacher(int code)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" DELETE FROM Teacher
                    WHERE TeacherCode = {code}");
        }

        public void UpdateTeacher(TeacherDto dto)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" UPDATE Teacher 
                    SET LastName={dto.LastName}, 
                        FirstName={dto.FirstName}, 
                        MiddleName={dto.MiddleName}  
                    WHERE TeacherCode={dto.TeacherCode} ");
        }

        public void AddPhone(int code, PhoneDto phone)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" INSERT INTO TeacherPhones
                    VALUES ({phone.Phone}, {code})");
        }

        public void AddEmail(int code, EmailDto email)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" INSERT INTO TeacherEmails
                        VALUES ({email.Email}, {code})");
        }

        public void DeletePhone(PhoneDto phone)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$"DELETE FROM TeacherPhones
                   WHERE PhoneNum = {phone.Phone}");
        }

        public void DeleteEmail(EmailDto email)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$"DELETE FROM TeacherEmails
                   WHERE Email = {email.Email}");
        }

        public void UpdatePhone(PhoneDto oldPhone, PhoneDto newPhone)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" UPDATE TeacherPhones
                    SET PhoneNum={newPhone.Phone}
                    WHERE PhoneNum={oldPhone.Phone}");
        }

        public void UpdateEmail(EmailDto oldEmail, EmailDto newEmail)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" UPDATE TeacherEmails
                    SET Email={newEmail.Email}
                    WHERE Email={oldEmail.Email}");
        }
    }
}