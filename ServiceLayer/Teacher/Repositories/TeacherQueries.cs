using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using DataLayer;
using DataLayer.Entities;
using DataLayer.QueryObjects;
using Microsoft.EntityFrameworkCore;
using ServiceLayer.Common;
using ServiceLayer.Common.EmailsPhones;
using ServiceLayer.Common.EmailsPhones.Dto;
using ServiceLayer.Common.EmailsPhones.Interfaces;
using ServiceLayer.Teacher.Dto;

namespace ServiceLayer.Teacher.Repositories
{
    public class TeacherQueries : IPhonesQueries, IEmailsQueries
    {
        private readonly SchoolContext _context;

        public TeacherQueries(SchoolContext context)
        {
            _context = context;
        }
        
        public IEnumerable<TeacherDto> GetTeachers()
        {
            return _context.Teachers.FromSqlRaw(
                "SELECT TeacherCode, LastName, FirstName, MiddleName " +
                "FROM Teacher").AsNoTracking().Select(t => new TeacherDto
            {
                TeacherCode = t.TeacherCode,
                FirstName = t.FirstName,
                LastName = t.LastName,
                MiddleName = t.MiddleName,
            }).ToImmutableList();
        }

        public IEnumerable<EmailDto> GetEmails(int code)
        {
            return _context.Set<Emails>().FromSqlInterpolated(
                $@"SELECT Email
                FROM TeacherEmails
                WHERE TeacherCode={code}").AsNoTracking().Select(email => new EmailDto { Email = email.Email }).ToImmutableList();
        }
        public IEnumerable<PhoneDto> GetPhones(int code)
        {
            return _context.Set<Phones>().FromSqlInterpolated(
                $@"SELECT PhoneNum
                FROM TeacherPhones
                WHERE TeacherCode={code}").AsNoTracking().Select(phone => new PhoneDto{Phone = phone.PhoneNum}).ToImmutableList();
        }

        public TeacherDto GetTeacher(int code)
        {
            var teacher = _context.Teachers.FromSqlInterpolated(
                $@" SELECT *
                    FROM Teacher
                    WHERE TeacherCode = {code}").AsNoTracking().First();

            return new TeacherDto
            {
                FirstName = teacher.FirstName,
                LastName = teacher.LastName,
                MiddleName = teacher.MiddleName,
                TeacherCode = teacher.TeacherCode
            };
        }
        
        public TeacherDto GetTeacher(string login)
        {
            var teacher = _context.Teachers.FromSqlInterpolated(
                $@" SELECT *
                    FROM Teacher
                    WHERE Login = {login}").AsNoTracking().First();

            return new TeacherDto
            {
                FirstName = teacher.FirstName,
                LastName = teacher.LastName,
                MiddleName = teacher.MiddleName,
                TeacherCode = teacher.TeacherCode
            };
        }
        
        public string GetLogin(int code)
        {
            return _context.Set<UserLogin>().FromSqlInterpolated(
                $@" SELECT Login
                    FROM Teacher
                    WHERE TeacherCode={code}").AsNoTracking().First().Login;
        }
    }
}