using DataLayer;
using Microsoft.EntityFrameworkCore;
using ServiceLayer.Family.Dto;

namespace ServiceLayer.Family.Repositories
{
    public class FamilyCommands
    {
        private readonly SchoolContext _context;

        public FamilyCommands(SchoolContext context)
        {
            _context = context;
        }
        
        public void CreateFamily(FamilyDto family)
        {
            _context.Database.ExecuteSqlInterpolated(
                $@" INSERT INTO Family(PupCode, RelCode, Type)
                    VALUES ({family.PupCode}, {family.RelCode}, {family.Type})");
        }

        public void DeleteFamily(int pupCode, int relCode)
        {
            _context.Database.ExecuteSqlInterpolated(
                $@" DELETE FROM Family
                    WHERE PupCode = {pupCode} AND RelCode = {relCode}");
        }
    }
}