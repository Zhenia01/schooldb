using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using DataLayer;
using DataLayer.QueryObjects;
using Microsoft.EntityFrameworkCore;
using ServiceLayer.Family.Dto;
using ServiceLayer.Pupil.Dto;
using ServiceLayer.Relative.Dto;

namespace ServiceLayer.Family.Repositories
{
    public class FamilyQueries
    {
        private readonly SchoolContext _context;

        public FamilyQueries(SchoolContext context)
        {
            _context = context;
        }

        public IEnumerable<FamilyPupilDto> GetFamilyPupils(int relCode)
        {
            return _context.Set<FamilyPupils>().FromSqlInterpolated(
                $@" SELECT F.PupCode, P.LastName, P.FirstName, F.Type
                    FROM Family AS F INNER JOIN Pupil AS P
                        ON P.PupCode = F.PupCode
                    WHERE F.RelCode = {relCode}"
            ).AsNoTracking().Select(fp => new FamilyPupilDto
            {
                FirstName = fp.FirstName,
                LastName = fp.LastName,
                PupCode = fp.PupCode,
                Type = fp.Type
            }).ToImmutableList();
        }

        public IEnumerable<FamilyRelativeDto> GetFamilyRelatives(int pupCode)
        {
            return _context.Set<FamilyRelatives>().FromSqlInterpolated(
                $@" SELECT F.RelCode, R.LastName, R.FirstName, F.Type
                    FROM Family AS F INNER JOIN Relative AS R
                        ON R.RelCode = F.RelCode
                    WHERE F.PupCode = {pupCode}"
            ).AsNoTracking().Select(fr => new FamilyRelativeDto
            {
                FirstName = fr.FirstName,
                LastName = fr.LastName,
                RelCode = fr.RelCode,
                Type = fr.Type
            }).ToImmutableList();
        }

        public IEnumerable<RelativeDto> GetNonFamilyRelatives(int pupCode)
        {
            return _context.Relatives.FromSqlInterpolated(
                $@" SELECT RelCode, LastName, FirstName, MiddleName
                    FROM Relative AS R
                    WHERE NOT EXISTS (
                        SELECT *
                        FROM Family
                        WHERE PupCode = {pupCode} AND RelCode = R.RelCode
                    )"
            ).AsNoTracking().Select(r => new RelativeDto
            {
                FirstName = r.FirstName,
                LastName = r.LastName,
                RelCode = r.RelCode, 
                MiddleName = r.MiddleName
            }).ToImmutableList();
        }
        
        public IEnumerable<PupilDto> GetNonFamilyPupils(int relCode)
        {
            return _context.Pupils.FromSqlInterpolated(
                $@" SELECT *
                    FROM Pupil AS P
                    WHERE NOT EXISTS (
                        SELECT *
                        FROM Family
                        WHERE RelCode = {relCode} AND PupCode = P.PupCode
                    )"
            ).AsNoTracking().Select(p => new PupilDto
            {
                FirstName = p.FirstName,
                LastName = p.LastName,
                PupCode = p.PupCode, 
                MiddleName = p.MiddleName
            }).ToImmutableList();
        }

        public bool IsFamily(int pupCode, int relCode)
        {
            return _context.Set<DataLayer.Entities.Family>().FromSqlInterpolated(
                $@" SELECT *
                    FROM Family
                    WHERE PupCode = {pupCode} AND RelCode = {relCode}"
            ).AsNoTracking().Any();
        }
    }
}