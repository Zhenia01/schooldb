namespace ServiceLayer.Family.Dto
{
    public class FamilyPupilDto
    {
        public int PupCode { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Type { get; set; }
    }
}