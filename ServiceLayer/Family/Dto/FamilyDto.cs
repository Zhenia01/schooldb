using System.ComponentModel.DataAnnotations;

namespace ServiceLayer.Family.Dto
{
    public class FamilyDto
    {
        [Required(ErrorMessage = "Учень - обов'язкове поле")]
        public int? PupCode { get; set; }
        [Required(ErrorMessage = "Родич - обов'язкове поле")]
        public int? RelCode { get; set; }
        
        [Required(ErrorMessage = "Тип - обов'язкове поле")]
        [MaxLength(10, ErrorMessage = "Максимальна довжина типу = 10")]
        public string Type { get; set; }
    }
}