using DataLayer;
using Microsoft.EntityFrameworkCore;
using ServiceLayer.Discipline.Dto;

namespace ServiceLayer.Discipline.Repositories
{
    public class DisciplineCommands
    {
        private readonly SchoolContext _context;

        public DisciplineCommands(SchoolContext context)
        {
            _context = context;
        }

        public void CreateDiscipline(DisciplineDto discipline)
        {
            _context.Database.ExecuteSqlInterpolated(
                $@" INSERT INTO Discipline (SubjName, `Group`, DateYear, AcademYear, Semester) 
                    VALUES ({discipline.SubjName}, {discipline.Group}, {discipline.DateYear}, {discipline.AcademYear}, {discipline.Semester})");
        }

        public void UpdateDiscipline(DisciplineDto dto)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" UPDATE Discipline
                    SET `Group`={dto.Group}, 
                        DateYear={dto.DateYear}, 
                        AcademYear={dto.AcademYear},
                        Semester={dto.Semester}  
                    WHERE DisCode={dto.DisCode} ");
        }

        public void DeleteDiscipline(int code)
        {
            _context.Database.ExecuteSqlInterpolated(
                @$" DELETE FROM Discipline
                    WHERE DisCode = {code}");
        }
    }
}