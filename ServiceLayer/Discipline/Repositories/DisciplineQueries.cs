using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using DataLayer;
using Microsoft.EntityFrameworkCore;
using ServiceLayer.Discipline.Dto;

namespace ServiceLayer.Discipline.Repositories
{
    public class DisciplineQueries
    {
        private readonly SchoolContext _context;

        public DisciplineQueries(SchoolContext context)
        {
            _context = context;
        }

        public IEnumerable<DisciplineDto> GetDisciplines()
        {
            return _context.Disciplines.FromSqlRaw(
                "SELECT * " +
                "FROM Discipline "
            ).AsNoTracking().Select(d => new DisciplineDto
            {
                DisCode = d.DisCode,
                SubjName = d.SubjName,
                Group = d.Group,
                Semester = d.Semester,
                AcademYear = d.AcademYear,
                DateYear = d.DateYear,
            }).ToImmutableList();
        }

        public int GetDisciplineCode(DisciplineDto dto)
        {
            return _context.Disciplines.FromSqlInterpolated(
                $@" SELECT *
                    FROM Discipline
                    WHERE SubjName = {dto.SubjName} 
                        AND `Group` = {dto.Group}
                        AND DateYear = {dto.DateYear} 
                        AND AcademYear = {dto.AcademYear} 
                        AND Semester = {dto.Semester} ").AsNoTracking().First().DisCode;
        }

        public DisciplineDto GetDiscipline(int code)
        {
            var discipline = _context.Disciplines.FromSqlInterpolated(
                $@" SELECT *
                    FROM Discipline
                    WHERE DisCode = {code}").AsNoTracking().First();

            return new DisciplineDto
            {
                Group = discipline.Group,
                Semester = discipline.Semester,
                DateYear = discipline.DateYear,
                DisCode = discipline.DisCode,
                AcademYear = discipline.AcademYear,
                SubjName = discipline.SubjName
            };
        }
    }
}