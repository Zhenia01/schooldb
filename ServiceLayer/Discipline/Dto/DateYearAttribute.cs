using System;
using System.ComponentModel.DataAnnotations;

namespace ServiceLayer.Discipline.Dto
{
    public class DateYearAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value is int dateYear)
            {
                int diff = DateTime.Now.Year - dateYear;
                return diff >= 0 && diff <= 12;
            }

            return false;
        }
    }
}