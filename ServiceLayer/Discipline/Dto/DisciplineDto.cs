using System.ComponentModel.DataAnnotations;

namespace ServiceLayer.Discipline.Dto
{
    public class DisciplineDto
    {
        public int DisCode { get; set; }
        
        [Required(ErrorMessage = "Предмет - обов'язкове поле")]
        [MaxLength(20, ErrorMessage = "Максимальна довжина предмету = 20")]
        public string SubjName { get; set; } // FK
        
        [Required(ErrorMessage = "Календарний рік - обов'язкове поле")]
        [DateYear(ErrorMessage = "Некоректний календарний рік")]
        public int DateYear { get; set; }
        
        [Required(ErrorMessage = "Навчальний рік - обов'язкове поле")]
        [Range(1, 11, ErrorMessage = "Навчальний рік - число від 1 до 12")]
        public int AcademYear { get; set; }
        
        [Required(ErrorMessage = "Семестр - обов'язкове поле")]
        [Range(1, 2, ErrorMessage = "Семестр - число від 1 до 2")]
        public int Semester { get; set; }
        
        [Required(ErrorMessage = "Група - обов'язкове поле")]
        [Range(1, int.MaxValue, ErrorMessage = "Група - число більше 1")]
        public int Group { get; set; }
    }
}