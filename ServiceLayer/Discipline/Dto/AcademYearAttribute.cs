using System.ComponentModel.DataAnnotations;

namespace ServiceLayer.Discipline.Dto
{
    public class AcademYearAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value is int academYear)
            {
                return academYear >= 1 && academYear <= 12;
            }

            return false;
        }
    }
}